'use strict';

function LoanComponent($scope) {

    var ctrl = this;
    $scope.selectLoan = () => {
        return { loanSelected: ctrl.loan }
    }
}

angular.module('home')
    .component('loanComponent', {
        templateUrl: 'home/loan/loan.component.html',
        bindings: {
            loan: '<',
            loanIdSelected: '<',
            onSelectLoan: '&'
        },
        controller: LoanComponent
    });