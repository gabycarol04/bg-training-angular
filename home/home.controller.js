'use strict';

function HomeController(Accounts, Loans, $scope) {

    $scope.isLoadingAccounts = true;
    $scope.isLoadingLoans = true;

    Accounts.getAll().then(function (result) {
        $scope.accounts = result.data;
        $scope.isLoadingAccounts = !$scope.isLoadingAccounts;
    }).catch(function(error) {
        $scope.isLoadingAccounts = !$scope.isLoadingAccounts;
    });

    Loans.getAll().then(function (result) {
        $scope.loans = result.data;
        $scope.isLoadingLoans = !$scope.isLoadingLoans;
    }).catch(function(error) {
        $scope.isLoadingLoans = !$scope.isLoadingLoans;
    });

    $scope.onSelectAccount = function (account) {
        $scope.accountSelected = account;
    }
}

angular
    .module('home')
    .controller('HomeController', ['Accounts', 'Loans', '$scope', HomeController]);