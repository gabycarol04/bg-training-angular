'use strict';

function AccountComponent($scope) {

    var ctrl = this;
    $scope.selectAccount = () => {
        return { accountSelectedFromComponent: ctrl.account }
    }
}

angular.module('home')
    .component('accountComponent', {
        templateUrl: 'home/account/account.component.html',
        bindings: {
            account: '<',
            accountIdSelected: '<',
            onSelectAccount: '&'
        },
        controller: AccountComponent
    });