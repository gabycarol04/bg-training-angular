'use strict';

function LoanDetailController($scope, $routeParams) {

    $scope.loanId = $routeParams.loanId;
}

angular
    .module('loanDetail')
    .controller('LoanDetailController', ['$scope', '$routeParams', LoanDetailController]);