'use strict';

angular.module('App')
    .component('transactionModalComponent', {
        templateUrl: 'transaction-modal/transaction-modal.component.html',
        bindings: {
            showModal: '=',
            icon: '<',
            iconStyle: '<',
            message: '<',
        }
    });