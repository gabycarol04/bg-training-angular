# Prueba de AngularJS

## Objetivo
El objetivo de esta prueba es que el estudiante pueda enfrentarse a un escenario real
y puedan resolver actividades como si se tratase de un hecho real.

> Nota: Lo primordial es realizar los ajustes de la mejor forma posible logrando el objetivo, se puede basar en búsquedas en Internet para lograr esto, sólo recuerda aplicar las mejoras prácticas. 

## Consideraciones
- Hacer uso de Visual Studio Code o algún editor de texto preferido para AngularJS.
- Hacer uso de la extensión [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) para Visual Studio Code.
- No está permitido compartir soluciones con otros colaboradores.
- Se trata de una prueba individual.
- El resultado de esta prueba será utilizado para evaluar los conocimientos adquiridos
  durante el curso (AngularJS, HTML5 y CSS3).
- La prueba tiene una duración de 2 horas.

## Pre requisitos
- Tener instalado GIT.
- Tener instalado NodeJS.
- Instalar de forma global `npm i -g stubby`.
- Crear un proyecto en su Bitbucket personal bajo el nombre: `angularjs-test` especificando la rama `master`.
- Clonar el proyecto Bitbucket: https://othercarlos93@bitbucket.org/othercarlos93/bg-training-angular.git .
- Entrar en el proyecto clonado.
- Renombrar el repositorio remoto `origin` a `old`.
- Agregar como repositorio remoto el proyecto personal creado en el primer paso con alias `origin`.
- Ejecutar el comando `git push -u origin master`.
- Crear una rama en su repositorio remoto personal llamada `develop`.
- Ejecutar en local el comando que actualiza el repositorio local con el remoto.
- En **local** cambiarse a la rama `develop` a partir de esta realizaremos los cambios.
- El proyecto viene con una carpeta `rest-mocks` donde están alojados los recursos de **stubby**, recuerde ejecutar `npm run start` dentro de dicha carpeta (Cambiar puerto si está en uso).

## Actividades
Se requiere que se solucionen varios aspectos de la página web escrita en AngularJS, HTML, CSS y Javascript.

### Incidente técnico
La aplicación no muestra ninguna información. ¿A qué se deberá?.
![IncidenteTecnico](./readme-resources/issue_0.png)

### Incidente 1
El cliente intenta visualizar el Detalle de sus cuentas pero al presionar la opción de **Ver Detalles** esta lo que realiza es un refrescamiento de la misma página. Se espera que se redireccione a la pantalla de **Detalle de la Cuenta**.
![IncidenteUno](./readme-resources/issue_1.png)

### Incidente 2
El cliente visualiza los **tipos de las cuentas en Inglés**, lo ideal es que se visualicen en español.
**(Tip: Crear Filter de AngularJS)**
![IncidenteDos](./readme-resources/issue_2.png)

### Incidente 3
Al presionar o hacer click sobre las tarjetas de Préstamos estas no realizan ninguna acción. Deberían de mostrar opciones como en el caso de las Cuentas.
![IncidenteTres](./readme-resources/issue_3.png)

### Mejora 1
Se requiere visualizar más detalle de los préstamos en la pantalla de **Detalle de Préstamo** (Ver detalle), mostrando información como:
- Cuota actual.
- Cuota total.
- Pago mensual del préstamo (Monto mensual).
- Número enmascarado.
- Tipo.
- Monto adeudado.
- Monto total.

El equipo de diseño deja los siguientes recursos:
**Pantalla 1 - Cargando**
![Feature1.1](./readme-resources/feature1-screen1.png)

**Pantalla 1 - Detalle de Préstamo**
![Feature1.2](./readme-resources/feature1-screen2.png)


> En el folder de `rest-mocks` haciendo uso de **stubby** existe un servicio que obtiene el detalle del préstamos por id de préstamo `/loans/{{loanId}}` con el método `GET`.

### Mejora 2
Se requiere que se permita pagar el préstamo seleccionado por el usuario, se deben visualizar los siguientes datos y consideraciones:
- En el home a la tarjeta de Préstamos se le debe visualizar la opción de **Pagar cuotas**
- Identificante del préstamo.
- Monto mensual asociado al préstamo.
- La opción de poder pagar entre el total adeudado y las cuotas individuales, con **Todas** por **default**.
  - Si se selecciona la opción **Todas** se deberá mostrar el **monto adeudado** y la opción de **Pagar**.
  - Si se selecciona la opción **Individualmente** se deberá listar cada una de las **Cuotas** asociadas al préstamo con su **monto total** y la opción de **Pagar**.
- La opción de pagar debe invocar a un servicio con el método `POST` en el `endpoints.json` se encuentra el API `/payments/loans/{{loanId}}`, se debe enviar el siguiente objeto
```
{
    "id": "3"
}
```
> Si se paga una cuota individualmente: El id debe corresponder al identificante de la cuota
> Si se paga el monto adeudado enviar cableado el texto `"owed"` en el id.

El equipo de Diseño deja las siguientes referencias al flujo:

**Pantalla 0 - Home - Opción de Pagar Cuotas**
![Feature2.1](./readme-resources/feature2-screen0.png)

**Pantalla 1 - Cargando**
![Feature2.2](./readme-resources/feature2-screen1.png)


**Pantalla 1 - Detalle de Préstamo - opcion todas**
![Feature2.3](./readme-resources/feature2-screen2.png)

**Pantalla 1 - Detalle de Préstamo - opcion individualmente**
![Feature2.4](./readme-resources/feature2-screen3.png)

**Pantalla 1 - Al ejecutar el pago**
![Feature2.5](./readme-resources/feature2-screen4.png)

> En el folder de `rest-mocks` haciendo uso de **stubby** existe un servicio que obtiene el detalle del préstamos por id de préstamo `/loans/{{loanId}}` con el método `GET`.
> En el folder de `rest-mocks` haciendo uso de **stubby** existe un servicio que ejecuta el pago del préstamo `/payments/loans/{{loanId}}` con el método `POST`.

## Material de apoyo

Varias de las actividades solicitadas fueron vistas en diferentes proyectos durante el curso, algunos de ellos versionados, de deja algunos links de referencia para realizar la prueba.

| Página | Links |
| ------ | ------ |
| AngularJS Tutorial | [https://docs.angularjs.org/tutorial] |
| AngularJS Developer Guide | [https://docs.angularjs.org/guide] |
| Bootstrap | [https://getbootstrap.com/docs/5.0/getting-started/introduction/] |
| Fontawesome | [https://fontawesome.com/v5.15/icons] |
| Proyecto ejemplo Phonecap | [https://github.com/angular/angular-phonecat.git] |
| Github: Proyecto component | [https://github.com/OtherCarlos/angujarjs-training-component] |
