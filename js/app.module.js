'use strict';

angular.module('App', [
    'ngRoute',
    'home',
    'accountDetail',
    'loanDetail',
    'core'
]);