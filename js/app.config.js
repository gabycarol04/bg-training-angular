'use strict';

angular
    .module('App')
    .config(['$routeProvider',
        function config($routeProvider) {
            $routeProvider.
                when('/', {
                    templateUrl: 'home/home.controller.html',
                    controller: 'HomeController'
                }).
                when('/account/:accountId', {
                    templateUrl: 'account-detail/account-detail.controller.html',
                    controller: 'AccountDetailController'
                }).
                when('/transfer/:accountId', {
                    templateUrl: 'account-detail/transfer/transfer.controller.html',
                    controller: 'TransferController'
                }).
                when('/loan/:loanId', {
                    templateUrl: 'loan-detail/loan-detail.controller.html',
                    controller: 'LoanDetailController'
                }).
                otherwise({ redirectTo: '/' });
        }
    ]);