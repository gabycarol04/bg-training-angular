'use strict';

function TransferController(Accounts, Beneficiaries, Transfer, $scope, $routeParams) {

    $scope.accountId = $routeParams.accountId;
    $scope.amount = '0.00';
    $scope.showTransactionAlert = false;
    $scope.isLoadingTransaction = [];
    
    $scope.isLoadingAccount = true;
    Accounts.get($routeParams.accountId).then(function (result) {
        $scope.account = result.data;
        $scope.isLoadingAccount = !$scope.isLoadingAccount;
    }).catch(function(error) {
        $scope.isLoadingAccount = !$scope.isLoadingAccount;
    });

    $scope.isLoadingBeneficiaries = true;
    Beneficiaries.getAll().then(function (result) {
        $scope.beneficiaries = result.data;
        $scope.isLoadingBeneficiaries = !$scope.isLoadingBeneficiaries;
    }).catch(function(error) {
        $scope.isLoadingBeneficiaries = !$scope.isLoadingBeneficiaries;
    });

    $scope.itOneTransactionExecuting = function() {
        return $scope.isLoadingTransaction.find(f => f === true);
    }

    $scope.transaction = function (beneficiary, arrayIndex) {
        $scope.isLoadingTransaction[arrayIndex] = true;
        Transfer.transaction($scope.account, beneficiary, $scope.amount).then(function () {
            $scope.isLoadingTransaction[arrayIndex] = !$scope.isLoadingTransaction[arrayIndex];
            $scope.iconName = 'fa-check-circle';
            $scope.iconStyle = 'text-success';
            $scope.showTransactionAlert = true;
        }).catch(function(error) {
            $scope.isLoadingTransaction[arrayIndex] = !$scope.isLoadingTransaction[arrayIndex];
            $scope.iconName = 'fa-times-circle';
            $scope.iconStyle = 'text-danger';
            $scope.transactionMessage = 'Error al ejecutar la transacción';
            $scope.showTransactionAlert = true;
        });
    }
}

angular
    .module('transfer')
    .controller('TransferController', ['Accounts', 'Beneficiaries', 'Transfer', '$scope', '$routeParams', TransferController]);