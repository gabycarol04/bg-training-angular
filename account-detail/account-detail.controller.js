'use strict';

function AccountDetailController(Accounts, $scope, $routeParams) {

    $scope.accountId = $routeParams.accountId;
    $scope.isLoadingAccount = true;

    Accounts.get($routeParams.accountId).then(function (result) {
        $scope.account = result.data;
        $scope.isLoadingAccount = !$scope.isLoadingAccount;
    }).catch(function(error) {
        $scope.isLoadingAccount = !$scope.isLoadingAccount;
    });
}

angular
    .module('accountDetail')
    .controller('AccountDetailController', ['Accounts', '$scope', '$routeParams', AccountDetailController]);