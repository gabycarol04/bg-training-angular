'use strict';

function Transfer($http) {
    const beneficiariesApi = '/transfer';
    this.transaction = function(account, beneficiary, amount) {
        return $http.post(`${baseUrl}${beneficiariesApi}`,
        { account: account, beneficiary: beneficiary, amount: amount });
    }

    return this;
}

angular
    .module('core')
    .factory('Transfer', ['$http', Transfer]);