'use strict';

function Accounts($http) {
    const accountsApi = '/accounts';
    this.getAll = function() {
        return $http.get(`${baseUrl}${accountsApi}`);
    }

    this.get = function(accountId) {
        return $http.get(`${baseUrl}${accountsApi}/${accountId}`);
    }

    return this;
}

angular
    .module('core')
    .factory('Accounts', ['$http', Accounts]);