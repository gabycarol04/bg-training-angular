'use strict';

function Loans($http) {
    const loansApi = '/loans';
    this.getAll = function() {
        return $http.get(`${baseUrl}${loansApi}`);
    }

    return this;
}

angular
    .module('core')
    .factory('Loans', ['$http', Loans]);