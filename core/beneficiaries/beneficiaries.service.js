'use strict';

function Beneficiaries($http) {
    const beneficiariesApi = '/beneficiaries';
    this.getAll = function() {
        return $http.get(`${baseUrl}${beneficiariesApi}`);
    }

    return this;
}

angular
    .module('core')
    .factory('Beneficiaries', ['$http', Beneficiaries]);